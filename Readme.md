1. Make the compile_commands: `mkdir build && cd build && CMAKE_EXPORT_COMPILE_COMMANDS=1 cmake .. -GNinja`
2. Compile with `cmake --build .`. Execute `./bad-includes`. See that `whatever` from `dog.h` could access `areCatsCute` from `cat.h`. 
3. Open this project in lvim and see that dog.h complains that it doesn't know what `areCatesCute` is.
